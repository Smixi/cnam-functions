import logging

import azure.functions as func
import json


def main(req: func.HttpRequest, documents: func.DocumentList) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')
    response = [{'filename': doc['name'], 'annotations': doc['text']} for doc in documents]
    return func.HttpResponse(
             json.dumps(response),
             status_code=200
        )
